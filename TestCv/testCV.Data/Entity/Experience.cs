﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Data.Entity
{
    public partial class Experience
    {
        [Key]
        public int ID { get; set; }
        public int ProfileID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        [Column(TypeName = "timestamp(6) without time zone")]
        public DateTime CreatedOn { get; set; }
        [Column(TypeName = "timestamp(6) without time zone")]
        public DateTime ModifiedOn { get; set; }

        [ForeignKey(nameof(ProfileID))]
        [InverseProperty("Experience")]
        public virtual Profile Profile { get; set; }
    }
}
