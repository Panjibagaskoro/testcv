﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Data.Entity
{
    public partial class TestCVContext:DbContext
    {
        public TestCVContext(DbContextOptions<TestCVContext> options)
            :base(options) { }

        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<Education> Education { get; set; }
        public virtual DbSet<Experience> Experience { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Experience>(entity =>
            {
                entity.HasOne(p => p.Profile)
                .WithMany(q => q.Experience)
                .HasForeignKey(p => p.ProfileID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Education>(entity =>
            {
                entity.HasOne(p => p.Profile)
                .WithMany(q => q.Education)
                .HasForeignKey(p => p.ProfileID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            OnModelCreatingPartial(modelBuilder);

        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    }
}
