﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Data.Entity
{
    public partial class Profile
    {
        public Profile()
        {
            Education = new HashSet<Education>();
            Experience = new HashSet<Experience>();
        }
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string DateOfBirth {  get; set; }
        [Column(TypeName = "timestamp(6) without time zone")]
        public DateTime CreatedOn { get; set; }
        [Column(TypeName = "timestamp(6) without time zone")]
        public DateTime ModifiedOn { get; set; }

        [InverseProperty("Profile")]
        public virtual ICollection<Education> Education { get; set; }


        [InverseProperty("Profile")]
        public virtual ICollection<Experience> Experience { get; set; }
    }
}
