﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCV.Api.Services;
using TestCV.Model;
using TestCV.Model.Common;

namespace TestCv.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _service;

        public ProfileController(IProfileService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<BaseResponse> Get()
        {
            BaseResponse response=await _service.Get();
            HttpContext.Response.StatusCode=response.code; 
            return response;
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<BaseResponse> Get(int id)
        {
            BaseResponse response = await _service.Get(id);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [AllowAnonymous]
        [HttpPost]
        public BaseResponse Create([FromBody]ProfileModel data)
        {
            BaseResponse response=_service.Create(data);
            HttpContext.Response.StatusCode=response.code;
            return response;
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public BaseResponse Update([FromBody]ProfileModel data,int id)
        {
            BaseResponse response=_service.Update(data,id);
            HttpContext.Response.StatusCode=response.code;
            return response;
        }


    }
}
