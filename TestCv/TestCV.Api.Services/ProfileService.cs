﻿
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Xml.Linq;
using TestCV.Data.Entity;
using TestCV.Model;
using TestCV.Model.Common;

namespace TestCV.Api.Services
{
    public interface IProfileService
    {
        Task<BaseResponse> Get();
        Task<BaseResponse> Get(int id);

        BaseResponse Create(ProfileModel data);
        BaseResponse Update(ProfileModel data,int id);
    }
    public class ProfileService:IProfileService
    {
        private readonly TestCVContext db;

        public ProfileService(TestCVContext ctx)
        {
            db = ctx;
        }

        public async Task<BaseResponse> Get(int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            try
            {
                var data = await (from a in db.Profile
                                  select new ProfileModel
                                  {
                                      Id = a.ID,
                                      name = a.Name,
                                      dob = a.DateOfBirth,
                                      educations = (from b in db.Education
                                                    where b.ProfileID == a.ID
                                                    select new EducationModel
                                                    {
                                                        name = b.Name,
                                                        start = b.Start,
                                                        end = b.End,
                                                        Id = b.ID
                                                    }
                                                  ).ToList(),
                                      experiences = (from c in db.Experience
                                                     where c.ProfileID == a.ID
                                                     select new ExperienceModel
                                                     {
                                                         id = c.ID,
                                                         company_name = c.Name,
                                                         description = c.Description,
                                                         end = c.End,
                                                         start = c.Start
                                                     }
                                                   ).ToList()
                                  }
                                ).FirstOrDefaultAsync();
                if (data != null)
                {
                    response = ResponseConstant.OK;
                    response.data = data;
                }
                else
                {
                    response = ResponseConstant.NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

        public async Task<BaseResponse> Get()
        {
            BaseResponse response = ResponseConstant.ERROR;
            try
            {
                var data = await (from a in db.Profile
                                  select new ProfileModel
                                  {
                                      Id = a.ID,
                                      name = a.Name,
                                      dob = a.DateOfBirth,
                                      educations = (from b in db.Education
                                                    where b.ProfileID == a.ID
                                                    select new EducationModel
                                                    {
                                                        name = b.Name,
                                                        start = b.Start,
                                                        end = b.End,
                                                        Id = b.ID
                                                    }
                                                  ).ToList(),
                                      experiences = (from c in db.Experience
                                                     where c.ProfileID == a.ID
                                                     select new ExperienceModel
                                                     {
                                                         id = c.ID,
                                                         company_name = c.Name,
                                                         description = c.Description,
                                                         end = c.End,
                                                         start = c.Start
                                                     }
                                                   ).ToList()
                                  }
                                ).ToListAsync();
                if(data != null )
                {
                    response = ResponseConstant.OK;
                    response.data = data;
                }
                else
                {
                    response = ResponseConstant.NOT_FOUND;
                }
            }
            catch (Exception ex)
            {
                response.message=ex.Message;
            }
            return response;
        }

        public BaseResponse Create(ProfileModel data)
        {
            BaseResponse response = ResponseConstant.ERROR;
            var check = db.Profile.Where(a => a.Name == data.name).FirstOrDefault();
            if(check==null)
            {
                using(var dbcxtransaction=db.Database.BeginTransaction())
                {
                    try
                    {
                        check = new Profile
                        {
                            Name = data.name,
                            DateOfBirth = data.dob,
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now
                        };
                        db.Profile.Add(check);
                        db.SaveChanges();

                        if(data.educations.Count>0 )
                        {
                            foreach(var education in data.educations)
                            {
                                Education item = new Education
                                {
                                    End = education.end,
                                    Start = education.start,
                                    Name = education.name,
                                    ProfileID = check.ID,
                                    CreatedOn = DateTime.Now,
                                    ModifiedOn = DateTime.Now
                                };
                                db.Education.Add(item);
                                db.SaveChanges();

                            }
                        }
                        
                        if(data.experiences.Count>0 )
                        {
                            foreach(var items in data.experiences)
                            {
                                Experience experience = new Experience
                                {
                                    ProfileID = check.ID,
                                    Name = items.company_name,
                                    Description = items.description,
                                    Start = items.start,
                                    End = items.end,
                                    CreatedOn = DateTime.Now,
                                    ModifiedOn = DateTime.Now
                                };
                                db.Experience.Add(experience);
                                db.SaveChanges();
                            }
                        }

                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;

                    }
                    catch(Exception e)
                    {
                        response.message = e.Message;
                    }
                }
                
            }
            return response;
        }

        public BaseResponse Update(ProfileModel data, int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            var check = db.Profile.Where(a=>a.ID==id).FirstOrDefault();
            if(check != null)
            {
                using (var dbcxtransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        check.Name = data.name;
                        check.DateOfBirth = data.dob;
                        check.ModifiedOn = DateTime.Now;
                        db.Entry(check).State=EntityState.Modified;
                        db.SaveChanges();

                        if(data.educations.Count>0)
                        {
                            foreach(var items in data.educations)
                            {
                                var getEducation = db.Education.Where(a=>a.ProfileID == id && a.ID == items.Id).FirstOrDefault();
                                if(getEducation != null)
                                {
                                    getEducation.Name = items.name;
                                    getEducation.Start = items.start;
                                    getEducation.End = items.end;
                                    getEducation.ModifiedOn = DateTime.Now;
                                    db.Entry(getEducation).State=EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }

                        if (data.experiences.Count > 0)
                        {
                            foreach(var items in data.experiences)
                            {
                                var getExperience = db.Experience.Where(a => a.ProfileID == id && a.ID == items.id).FirstOrDefault();
                                if (getExperience != null)
                                {
                                    getExperience.Name = items.company_name;
                                    getExperience.Start = items.start;
                                    getExperience.End = items.end;
                                    getExperience.ModifiedOn = DateTime.Now;
                                    db.Entry(getExperience).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;




                    }
                    catch(Exception e)
                    {
                        response.message = e.Message;
                    }
                }
                    
            }
            return response;
        }

    }
}