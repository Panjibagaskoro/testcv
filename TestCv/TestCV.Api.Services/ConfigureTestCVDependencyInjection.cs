﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Api.Services
{
    public static class ConfigureDependencyInjection
    {
        public static IServiceCollection ConfigureTestCVDependencyInjection(this IServiceCollection services)
        {
            services.AddScoped<IProfileService,ProfileService>();
            return services;
        }
    }
}
