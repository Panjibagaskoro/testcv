﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Model
{
    public class ExperienceModel
    {
        public int id { get; set; }
        public string company_name { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string description { get;set; }
    }
}
