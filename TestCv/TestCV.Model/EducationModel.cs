﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCV.Model
{
    public class EducationModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string start { get; set; }
        public string end { get; set; }
    }
}
