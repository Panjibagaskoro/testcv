﻿namespace TestCV.Model
{
    public class ProfileModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string dob { get; set; }
        public List<ExperienceModel> experiences { get; set; }
        public List<EducationModel> educations { get; set; }

    }
}