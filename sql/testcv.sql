--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2024-02-06 00:22:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3015 (class 1262 OID 24599)
-- Name: testcv; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE testcv WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Indonesia.1252';


ALTER DATABASE testcv OWNER TO postgres;

\connect testcv

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 205 (class 1259 OID 24621)
-- Name: Education; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Education" (
    "ID" integer NOT NULL,
    "Name" character varying(255) NOT NULL,
    "Start" character varying(40) NOT NULL,
    "End" character varying(40) NOT NULL,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "ProfileID" integer NOT NULL
);


ALTER TABLE public."Education" OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24619)
-- Name: Education_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Education_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Education_ID_seq" OWNER TO postgres;

--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 204
-- Name: Education_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Education_ID_seq" OWNED BY public."Education"."ID";


--
-- TOC entry 203 (class 1259 OID 24610)
-- Name: Experience; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Experience" (
    "ID" integer NOT NULL,
    "Name" character varying(255) NOT NULL,
    "Description" text NOT NULL,
    "Start" character varying(40) NOT NULL,
    "End" character varying(40) NOT NULL,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone,
    "ProfileID" integer NOT NULL
);


ALTER TABLE public."Experience" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24608)
-- Name: Experience_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Experience_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Experience_ID_seq" OWNER TO postgres;

--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 202
-- Name: Experience_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Experience_ID_seq" OWNED BY public."Experience"."ID";


--
-- TOC entry 201 (class 1259 OID 24602)
-- Name: Profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Profile" (
    "ID" integer NOT NULL,
    "Name" character varying(255) NOT NULL,
    "DateOfBirth" character varying(40) NOT NULL,
    "CreatedOn" timestamp without time zone,
    "ModifiedOn" timestamp without time zone
);


ALTER TABLE public."Profile" OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24600)
-- Name: Profile_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Profile_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Profile_ID_seq" OWNER TO postgres;

--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 200
-- Name: Profile_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Profile_ID_seq" OWNED BY public."Profile"."ID";


--
-- TOC entry 2865 (class 2604 OID 24624)
-- Name: Education ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Education" ALTER COLUMN "ID" SET DEFAULT nextval('public."Education_ID_seq"'::regclass);


--
-- TOC entry 2864 (class 2604 OID 24613)
-- Name: Experience ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Experience" ALTER COLUMN "ID" SET DEFAULT nextval('public."Experience_ID_seq"'::regclass);


--
-- TOC entry 2863 (class 2604 OID 24605)
-- Name: Profile ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile" ALTER COLUMN "ID" SET DEFAULT nextval('public."Profile_ID_seq"'::regclass);


--
-- TOC entry 3009 (class 0 OID 24621)
-- Dependencies: 205
-- Data for Name: Education; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Education" ("ID", "Name", "Start", "End", "CreatedOn", "ModifiedOn", "ProfileID") VALUES (1, 'test', '2012', '2016', '2024-02-06 00:15:28.387418', '2024-02-06 00:15:28.396374', 1);
INSERT INTO public."Education" ("ID", "Name", "Start", "End", "CreatedOn", "ModifiedOn", "ProfileID") VALUES (2, 'test', '2012', '2016', '2024-02-06 00:17:27.332565', '2024-02-06 00:17:27.342993', 2);


--
-- TOC entry 3007 (class 0 OID 24610)
-- Dependencies: 203
-- Data for Name: Experience; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Experience" ("ID", "Name", "Description", "Start", "End", "CreatedOn", "ModifiedOn", "ProfileID") VALUES (1, 'PT Abishar Technologies', 'test', 'August 2022', 'NOW', '2024-02-06 00:15:31.241327', '2024-02-06 00:15:31.24143', 1);
INSERT INTO public."Experience" ("ID", "Name", "Description", "Start", "End", "CreatedOn", "ModifiedOn", "ProfileID") VALUES (2, 'PT Abishar Technologies', 'test', 'August 2022', 'NOW', '2024-02-06 00:17:30.974665', '2024-02-06 00:17:30.982308', 2);


--
-- TOC entry 3005 (class 0 OID 24602)
-- Dependencies: 201
-- Data for Name: Profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Profile" ("ID", "Name", "DateOfBirth", "CreatedOn", "ModifiedOn") VALUES (1, 'Panji Bagaskoro', '08-08-1994', '2024-02-06 00:15:25.007517', '2024-02-06 00:15:25.016577');
INSERT INTO public."Profile" ("ID", "Name", "DateOfBirth", "CreatedOn", "ModifiedOn") VALUES (2, 'Panji Bagaskoroo', '08-08-1994', '2024-02-06 00:17:23.66572', '2024-02-06 00:17:23.674272');


--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 204
-- Name: Education_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Education_ID_seq"', 2, true);


--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 202
-- Name: Experience_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Experience_ID_seq"', 2, true);


--
-- TOC entry 3021 (class 0 OID 0)
-- Dependencies: 200
-- Name: Profile_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Profile_ID_seq"', 2, true);


--
-- TOC entry 2871 (class 2606 OID 24626)
-- Name: Education Education_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Education"
    ADD CONSTRAINT "Education_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 2869 (class 2606 OID 24618)
-- Name: Experience Experience_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Experience"
    ADD CONSTRAINT "Experience_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 2867 (class 2606 OID 24607)
-- Name: Profile Profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Profile"
    ADD CONSTRAINT "Profile_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 2873 (class 2606 OID 24632)
-- Name: Education Education_ProfileID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Education"
    ADD CONSTRAINT "Education_ProfileID_fkey" FOREIGN KEY ("ProfileID") REFERENCES public."Profile"("ID") ON DELETE CASCADE;


--
-- TOC entry 2872 (class 2606 OID 24627)
-- Name: Experience Experience_ProfileID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Experience"
    ADD CONSTRAINT "Experience_ProfileID_fkey" FOREIGN KEY ("ProfileID") REFERENCES public."Profile"("ID") ON DELETE CASCADE;


-- Completed on 2024-02-06 00:22:36

--
-- PostgreSQL database dump complete
--

